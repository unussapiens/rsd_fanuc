// RobotComms.cpp : main project file.

/******************************************************************************\
|  Includes                                                                    |
\******************************************************************************/

#include <Windows.h>

#include "stdafx.h"

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <list>
#include <vector>
#include <stdio.h>

using namespace cv;
using namespace std;

using namespace System;
using namespace System::IO::Ports;

/******************************************************************************\
|  Global Variables                                                            |
\******************************************************************************/

int low_threshold = 10;
int max_low_threshold = 255;

int high_threshold = 200;
int max_high_threshold = 255;

int corner_index = 0;
int work_area_width = 200;
int work_area_height = 255;

// Coords of work area in webcam FOV
Point2f work_area_corners[4] = { Point2f(0,0), Point2f(640,0), Point2f(0,480), Point2f(640,480) };

// Coords to transform to for the work area
Point2f work_area_targets[4] = { Point2f(0,0),
								Point2f((float)work_area_height,0),
								Point2f(0,(float)work_area_width),
								Point2f((float)work_area_height,(float)work_area_width)
};

// Work area transformation matrices
Mat perspective_matrix;
Mat inv_perspective_matrix;

// Seed the random number generator
RNG rng(12345);

// Kernel for blurring the image
// Should be one of 1,3,5,7
#define KERNEL_SIZE			(3)

// The number of edges that denotes each shape
#define NUM_POLYS_SQUARE	(4)
#define NUM_POLYS_CIRCLE	(8)

// Number of frames to merge to before processing - 15 seems to work well
#define NUM_FRAMES_AVG		(10)

/******************************************************************************\
|  Functions                                                                   |
\******************************************************************************/

//hide the local functions in an anon namespace
namespace {
	//----------------------------------------------------------------------------
	// Used to catch mouse clicks to define the bounding box of the work area
	//----------------------------------------------------------------------------
	static void onMouse_callback(int event, int x, int y, int, void*)
	{
		// Only respond on the release of the left button
		if (event != EVENT_LBUTTONUP)
		{
			return;
		}

		// Set the clicked point as the next corner
		work_area_corners[corner_index] = Point2f(float(x), float(y));

		cout << "Corner " << corner_index << ": " << x << ", " << y << endl;

		if (corner_index == 3)
		{
			// Full set of corners received - calculate perspective matrix
			perspective_matrix = getPerspectiveTransform(work_area_corners, work_area_targets);
			inv_perspective_matrix = getPerspectiveTransform(work_area_targets, work_area_corners);
		}

		// Increment the index of the next corner, with looping as required
		corner_index = (corner_index + 1) % 4;
	}

	//----------------------------------------------------------------------------
	// Finds circles and squares within a region (defined by the perspective
	// matrix) in the provided frame.
	//----------------------------------------------------------------------------
	Mat locate_objects(Mat frame, Mat perspective_matrix, vector<Point> &circles_out, vector<Point> &squares_out)
	{
		Mat canny_output;
		vector<vector<Point>> contours;
		vector<vector<Point>> poly_contours;
		vector<Vec4i> hierachy;

		// Transform to get work area only from webcam image
		Mat work_area;
		warpPerspective(frame, work_area, perspective_matrix, Size(work_area_height, work_area_width));

		// Convert image to grayscale
		Mat work_area_gray;
		cvtColor(work_area, work_area_gray, CV_BGR2GRAY);

		// Blur it using a normalized box filter
		blur(work_area_gray, work_area_gray, Size(KERNEL_SIZE, KERNEL_SIZE));

		// Detect edges using Canny
		Canny(work_area_gray, canny_output, low_threshold, high_threshold, 3);

		// Find contours
		findContours(canny_output, contours, hierachy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));

		poly_contours.resize(contours.size());

		// Find the centre of each contour and add it to a list
		for (size_t k = 0; k < contours.size(); k++)
		{
			approxPolyDP(contours[k], poly_contours[k], 3, true);

			// Calculate the centre as the mean of the points
			Point centre;
			for (size_t i = 0; i < poly_contours[k].size(); i++)
			{
				centre += poly_contours[k][i];
			}
			centre *= (1.0 / poly_contours[k].size());

			// Check if its a square or circle and add it to the appropriate list
			if (abs((int)(poly_contours[k].size() - NUM_POLYS_SQUARE)) <= 1)
			{
				squares_out.push_back(centre);
			}

			if (poly_contours[k].size() >= NUM_POLYS_CIRCLE - 2)
			{
				circles_out.push_back(centre);
			}
		}

		// Draw contours
		Mat drawing = Mat::zeros(canny_output.size(), CV_8UC3);

		for (int i = 0; i < poly_contours.size(); i++)
		{
			Scalar color = Scalar(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255));
			drawContours(drawing, poly_contours, i, color, 2, 8, hierachy, 0, Point());
		}

		for (int i = 0; i < circles_out.size(); i++)
		{
			Scalar color = Scalar(255, 0, 0);
			circle(drawing, circles_out[i], 5, color);
		}

		for (int i = 0; i < squares_out.size(); i++)
		{
			Scalar color = Scalar(0, 255, 0);
			circle(drawing, squares_out[i], 5, color);
		}

		return drawing;
	}

	//----------------------------------------------------------------------------
	// Equalizes the histogram of the provided image.
	//----------------------------------------------------------------------------
	Mat equalizeIntensity(const Mat& input)
	{
		if (input.channels() >= 3)
		{
			Mat ycrcb;

			cvtColor(input, ycrcb, CV_BGR2YCrCb);

			vector<Mat> channels;
			split(ycrcb, channels);

			equalizeHist(channels[0], channels[0]);

			Mat result;
			merge(channels, ycrcb);

			cvtColor(ycrcb, result, CV_YCrCb2BGR);

			return result;
		}
		return Mat();
	}

	//----------------------------------------------------------------------------
	// Sends a byte of data to the robot
	//----------------------------------------------------------------------------
	void serial_send(SerialPort^ robot_int, unsigned char data)
	{
		if (robot_int->IsOpen)
		{
			array<unsigned char>^ char_array = gcnew array<unsigned char>(1);
			char_array[0] = data;

			robot_int->Write(char_array, 0, 1);
		}
	}

	//----------------------------------------------------------------------------
	// Sends a Point to the robot as two bytes seperated by a zero.
	// x and y coordinates must be between 1 and 255 inclusive.
	//----------------------------------------------------------------------------
	void serial_send_coords(SerialPort^ robot_int, Point coord)
	{
		serial_send(robot_int, coord.y);
		Sleep(1000);
		serial_send(robot_int, 0);
		Sleep(1000);
		serial_send(robot_int, coord.x);
		Sleep(1000);
		serial_send(robot_int, 0);
	}

	//----------------------------------------------------------------------------
	// Loops to read in the camera feed and show detected objects.
  // Also allows images to be saved.
	//----------------------------------------------------------------------------
	int process(VideoCapture& capture, SerialPort^ robot_int)
	{

		cout << "press space to save a picture. q or esc to quit" << endl;

		// Variables for saving images
		int n = 0;
		char filename[200];

		// Create the windows
		string raw_window_name = "video | q or esc to quit";
		string contour_window_name = "Contours";
		namedWindow(raw_window_name, CV_WINDOW_KEEPRATIO);
		namedWindow(contour_window_name, CV_WINDOW_KEEPRATIO);

		// Create trackbars
		createTrackbar("Low Thresh:", contour_window_name, &low_threshold, max_low_threshold);
		createTrackbar("Hi  Thresh:", contour_window_name, &high_threshold, max_high_threshold);

		// Set the initial perspective matrix
		perspective_matrix = getPerspectiveTransform(work_area_corners, work_area_targets);
		inv_perspective_matrix = getPerspectiveTransform(work_area_targets, work_area_corners);
		setMouseCallback(raw_window_name, onMouse_callback);

		// Create the variables to hold frames
		Mat frame_eq;
		Mat frame_raw;
		Mat frame_drawn;
		list<Mat> frames;

		for (;;) {
			capture >> frame_raw;
			if (frame_raw.empty())
			{
				break;
			}

			frame_eq = equalizeIntensity(frame_raw);

			array<unsigned char>^ texBufArray = gcnew array<unsigned char>(1);

			//
			// Update the frame buffer and calculate the average
			//

			Mat frame_avg(frame_eq.rows, frame_eq.cols, CV_8UC3, Scalar(0, 0, 0));

			frames.push_back(frame_eq.clone());

			// Remove a frame if the buffer is full
			if (frames.size() > NUM_FRAMES_AVG)
			{
				frames.pop_front();
			}

			// Sum and then average the frames
			for (list<Mat>::iterator iter = frames.begin(); iter != frames.end(); iter++)
			{
				frame_avg = (frame_avg + *iter) * (1.0 / 2.0);
			}

			//
			// Locate objects within the work window
			//

			vector<Point>  circles_work;
			vector<Point2f> circles_work_2f;
			vector<Point2f> circles_global;

			vector<Point>  squares_work;
			vector<Point2f> squares_work_2f;
			vector<Point2f> squares_global;

			Mat objects_frame = locate_objects(frame_avg, perspective_matrix, circles_work, squares_work);

			// Convert the work-area coordinates to floating point
			circles_work_2f.resize(circles_work.size());
			squares_work_2f.resize(squares_work.size());

			for (int i = 0; i < circles_work.size(); i++)
			{
				circles_work_2f[i] = circles_work[i];
			}

			for (int i = 0; i < squares_work.size(); i++)
			{
				squares_work_2f[i] = squares_work[i];
			}

			circles_global.resize(circles_work.size());
			squares_global.resize(squares_work.size());

			// Transform the work-area coordinates to global coordinates
			try {
				perspectiveTransform(circles_work_2f, circles_global, inv_perspective_matrix);
				perspectiveTransform(squares_work_2f, squares_global, inv_perspective_matrix);
			}
			catch (exception e) {
				// TODO Figure out why these exceptions are happening
			}

			//
			// Draw the output stream located objects
			//
			frame_drawn = frame_avg.clone();

			for (int i = 0; i < circles_global.size(); i++)
			{
				Scalar color = Scalar(0, 0, 255);
				circle(frame_drawn, circles_global[i], 10, color);
			}
			for (int i = 0; i < squares_global.size(); i++)
			{
				Scalar color = Scalar(0, 255, 0);
				circle(frame_drawn, squares_global[i], 5, color);
			}
			//
			// Draw work area rectangle
			//

			// Draw a border around the work area if it is fully defined
			if (corner_index == 0)
			{
				line(frame_drawn, work_area_corners[0], work_area_corners[1], Scalar(0, 0, 0), 2);
				line(frame_drawn, work_area_corners[0], work_area_corners[2], Scalar(0, 0, 0), 2);
				line(frame_drawn, work_area_corners[2], work_area_corners[3], Scalar(0, 0, 0), 2);
				line(frame_drawn, work_area_corners[1], work_area_corners[3], Scalar(0, 0, 0), 2);
			}

			// Draw the windows
			imshow(raw_window_name, frame_drawn);
			imshow(contour_window_name, objects_frame);

			char key = (char)waitKey(5); //delay N millis, usually long enough to display and capture input
			switch (key) {
			case 'q':
			case 'Q':
			case 27: //escape key
				return 0;
			case ' ': //Save an image
				//sprintf(filename,"filename%.3d.jpg",n++);
				imwrite(filename, frame_raw);
				cout << "Saved " << filename << endl;
				break;
			default:
				break;
			}
		}
		return 0;
	}

	//----------------------------------------------------------------------------
	// Allows the user to define a work area.
  // Detects objects to draw their position, but does not return anything.
	//----------------------------------------------------------------------------
	void define_work_area(VideoCapture& capture)
	{
		cout << "press space to save a picture. q or esc to quit" << endl;

		// Create the windows
		string raw_window_name = "video | space when finished";
		string contour_window_name = "Contours";
		namedWindow(raw_window_name, CV_WINDOW_KEEPRATIO);
		namedWindow(contour_window_name, CV_WINDOW_KEEPRATIO);

		// Create trackbars
		createTrackbar("Low Thresh:", contour_window_name, &low_threshold, max_low_threshold);
		createTrackbar("Hi  Thresh:", contour_window_name, &high_threshold, max_high_threshold);

		// Set the initial perspective matrix
		perspective_matrix = getPerspectiveTransform(work_area_corners, work_area_targets);
		inv_perspective_matrix = getPerspectiveTransform(work_area_targets, work_area_corners);
		setMouseCallback(raw_window_name, onMouse_callback);

		// Create the variables to hold frames
		Mat frame_raw;
		Mat frame_drawn;
		list<Mat> frames;

		for (;;) {
			capture >> frame_raw;
			if (frame_raw.empty())
			{
				break;
			}

			array<unsigned char>^ texBufArray = gcnew array<unsigned char>(1);

			//
				// Update the frame buffer and calculate the average
				//

			Mat frame_avg(frame_raw.rows, frame_raw.cols, CV_8UC3, Scalar(0, 0, 0));

			frames.push_back(frame_raw.clone());

			// Remove a frame if the buffer is full
			if (frames.size() > NUM_FRAMES_AVG)
			{
				frames.pop_front();
			}

			// Sum and then average the frames
			for (list<Mat>::iterator iter = frames.begin(); iter != frames.end(); iter++)
			{
				frame_avg = (frame_avg + *iter) * (1.0 / 2.0);
			}

			//
			// Locate objects within the work window
			//

			vector<Point>  circles_work;
			vector<Point2f> circles_work_2f;
			vector<Point2f> circles_global;

			vector<Point>  squares_work;
			vector<Point2f> squares_work_2f;
			vector<Point2f> squares_global;

			Mat objects_frame = locate_objects(frame_avg, perspective_matrix, circles_work, squares_work);

			// Convert the work-area coordinates to floating point
			circles_work_2f.resize(circles_work.size());
			squares_work_2f.resize(squares_work.size());

			for (int i = 0; i < circles_work.size(); i++)
			{
				circles_work_2f[i] = circles_work[i];
			}

			for (int i = 0; i < squares_work.size(); i++)
			{
				squares_work_2f[i] = squares_work[i];
			}

			circles_global.resize(circles_work.size());
			squares_global.resize(squares_work.size());

			// Transform the work-area coordinates to global coordinates
			try {
				perspectiveTransform(circles_work_2f, circles_global, inv_perspective_matrix);
				perspectiveTransform(squares_work_2f, squares_global, inv_perspective_matrix);
			}
			catch (exception e) {
				// TODO Figure out why these exceptions are happening
				std::cout << "Exception!" << std::endl;
			}

			//
			// Draw the output stream located objects
			//

			frame_drawn = frame_avg.clone();

			for (int i = 0; i < circles_global.size(); i++)
			{
				Scalar color = Scalar(0, 0, 255);
				circle(frame_drawn, circles_global[i], 10, color);
			}

			for (int i = 0; i < squares_global.size(); i++)
			{
				Scalar color = Scalar(0, 255, 0);
				circle(frame_drawn, squares_global[i], 5, color);
			}

			//
			// Draw work area rectangle
			//

			// Draw a border around the work area if it is fully defined
			if (corner_index == 0)
			{
				line(frame_drawn, work_area_corners[0], work_area_corners[1], Scalar(0, 0, 0), 2);
				line(frame_drawn, work_area_corners[0], work_area_corners[2], Scalar(0, 0, 0), 2);
				line(frame_drawn, work_area_corners[2], work_area_corners[3], Scalar(0, 0, 0), 2);
				line(frame_drawn, work_area_corners[1], work_area_corners[3], Scalar(0, 0, 0), 2);
			}

			// Draw the windows
			imshow(raw_window_name, frame_drawn);
			imshow(contour_window_name, objects_frame);

			char key = (char)waitKey(5); //delay N millis, usually long enough to display and capture input
			switch (key) {
			case 'q':
			case 'Q':
			case 27: //escape key
				return;
			case ' ': //Save an image
				return;
			default:
				break;
			}
		}
	}

	//-----------------------------------------------------------------------------
  // Get to coordinates of an object detected from the webcam feed.
  // num_frames is the number of frames to average for the analysis.
	//-----------------------------------------------------------------------------
	Point get_object_coord(VideoCapture& capture, unsigned int num_frames)
	{
		// Create the variables to hold frames
		Mat frame_raw;
		Mat frame_drawn;
		list<Mat> frames;

		while (num_frames--)
		{
			capture >> frame_raw;
			if (frame_raw.empty())
			{
				break;
			}

			// Add it to the frame buffer
			frames.push_back(frame_raw.clone());

			Sleep(5);
		}

		Mat frame_avg(frame_raw.rows, frame_raw.cols, CV_8UC3, Scalar(0, 0, 0));

		std::cout << "Averaging frames" << std::endl;

		// Sum and then average the frames
		for (list<Mat>::iterator iter = frames.begin(); iter != frames.end(); iter++)
		{
			frame_avg = (frame_avg + *iter) * (1.0 / 2.0);
		}

		//
		// Locate objects within the work window
		//

		vector<Point>  circles_work;
		vector<Point2f> circles_work_2f;
		vector<Point2f> circles_global;

		vector<Point>  squares_work;
		vector<Point2f> squares_work_2f;
		vector<Point2f> squares_global;

		std::cout << "Location objects... ";

		Mat objects_frame = locate_objects(frame_avg, perspective_matrix, circles_work, squares_work);

		std::cout << "Done" << std::endl;

		// Convert the work-area coordinates to floating point
		circles_work_2f.resize(circles_work.size());
		squares_work_2f.resize(squares_work.size());

		for (int i = 0; i < circles_work.size(); i++)
		{
			circles_work_2f[i] = circles_work[i];
		}

		for (int i = 0; i < squares_work.size(); i++)
		{
			squares_work_2f[i] = squares_work[i];
		}

		circles_global.resize(circles_work.size());
		squares_global.resize(squares_work.size());
		/*
		// Transform the work-area coordinates to global coordinates
		try {
			perspectiveTransform(circles_work_2f, circles_global, inv_perspective_matrix);
			perspectiveTransform(squares_work_2f, squares_global, inv_perspective_matrix);
		}
		catch (exception e) {
			// TODO Figure out why these exceptions are happening
		}

		//
		// Draw the output stream located objects
		//

		frame_drawn = frame_avg.clone();

		for (int i = 0; i < circles_global.size(); i++)
		{
			Scalar color = Scalar(0, 0, 255);
			circle(frame_drawn, circles_global[i], 10, color);
		}

		for (int i = 0; i < squares_global.size(); i++)
		{
			Scalar color = Scalar(0, 255, 0);
			circle(frame_drawn, squares_global[i], 5, color);
		}

		// Draw a border around the work area if it is fully defined
		if (corner_index == 0)
		{
			line(frame_drawn, work_area_corners[0], work_area_corners[1], Scalar(0, 0, 0), 2);
			line(frame_drawn, work_area_corners[0], work_area_corners[2], Scalar(0, 0, 0), 2);
			line(frame_drawn, work_area_corners[2], work_area_corners[3], Scalar(0, 0, 0), 2);
			line(frame_drawn, work_area_corners[1], work_area_corners[3], Scalar(0, 0, 0), 2);
		}
		*/
		// Return a location

		if (circles_work.size() > 0)
		{
			return circles_work.front();
		}
		else
		{
			return Point(0, 0);
		}
	}
}

int main(int ac, char** av)
{
	// robot interpreter box settings
	int baudRate = 9600;
	SerialPort^ robot_int;
	robot_int = gcnew SerialPort("COM4", baudRate);

	std::string arg;

	if (ac != 2) {
		cout << "Assuming  video source 0..." << endl;
		arg = "0";
	}
	else {
		arg = av[1];
	}

	VideoCapture capture(arg);  //try to open string, this will attempt to open it as a video file
	if (!capture.isOpened())    //if this fails, try to open as a video camera, through the use of an integer param
		capture.open(atoi(arg.c_str()));
	if (!capture.isOpened()) {
		cerr << "Failed to open a video device or video file!\n" << endl;
		help(av);
		return 1;
	}

	try
	{
		robot_int->Open();
	}
	catch (IO::IOException^ e)
	{
		cerr << "Failed to open serial port to I/O interface!\n" << endl;
		cerr << "Running without serial comms" << endl;
	}

	define_work_area(capture);

	bool finished = false;

	while (!finished)
	{
		std::cout << "Start of loop" << std::endl;
		Point coords = get_object_coord(capture, 20);
		std::cout << "Got coords" << std::endl;

		std::cout << coords << std::endl;
		serial_send_coords(robot_int, coords);

		std::string str;
		cin >> str;

		if (str == "q")
		{
			finished = true;
		}
		std::cout << "End of loop" << std::endl;
	}
}
